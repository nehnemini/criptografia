/* Universidad Nacional Autónoma de México */
/* Paulo Contreras Flores */
/* paulo.contreras.flores@ciencias.unam.mx */

// compilar gcc aes_keysched.c -o aes_keysched.o

#include <stdio.h>
#include <stdlib.h>
#include "aes.h"



void main(){

    B8 key[16]    = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 
                     0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
    B16 keylength = 128;
 

    KeyBlockRound( keylength );
    KeyExpansion( key, w, Nk);
    getSboxCoordExample();

}



void KeyExpansion( B8 key[4*Nk], B32 w[Nb*(Nr+1)], B16 Nk ){

    B32 temp;
    B16 i = 0;


   while(i < Nk){
       // corrimiento izq 8, 16 o 24 bits para unirlos en 32 bits 
       w[i] = ( (key[4*i])   << 24) | ((key[4*i+1]) << 16) |
                ((key[4*i+2]) << 8) | ((key[4*i+3])          ); 

       if(debug){
           printf("\t%x\n",       key[4*i]  <<24 );
           printf("\t  %x\n",     key[4*i+1]<<16 );
           printf("\t    %x\n",   key[4*i+2]<<8  );
           printf("\t      %x\n", key[4*i+3]     );
		   printf("-------------------\n");
           printf(" w[%i] = %x\n\n", i,w[i]);
       }
       i = i + 1;
   }



   /*  Aquí va el resto del código */



}



void KeyBlockRound(B16 keylength){

    switch(keylength){
        case 128: Nk = 4; Nr = 10; break;
        case 192: Nk = 6; Nr = 12; break;
        case 256: Nk = 8; Nr = 14; break;
        default: printf("Error-longitud de llave incorrecto\n");
    }

}


void getSboxCoordExample(){


    B32 x;
    B32 y;

    B32 temp = 0xcf4f3c09;

    printf("\n\nOperación AND entre\n");
    printf("temp = %x\n", temp);
    printf("mask = f0000000 \n");
    printf("----------\n");

    x = temp & 0xf0000000;
    printf("   x = %x\n",x);

    printf("\nCorrimiento de 28 bits a la derecha para quitar ceros\n");
    x = x >> 28;
    printf("    x = %x\n",x);

   printf("\nOperación AND y corrimiento a la derecha de 24 bits, en un solo paso\n");
   printf("temp = %x\n", temp);
   printf("mask = 0f000000 \n");
   printf("----------\n");

   y = (temp & 0x0f000000) >> 24;
   printf("    y = %x\n",y);


  
}
