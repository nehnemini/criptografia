El archivo Dockerfile, sin extensión, contiene las instrucciones necesarias para la construcción de la imagen y posterior ejecución del programa.

Para probar si la construcción de la imagen es correcta siga los siguientes pasos.

1. Instale Docker, si se está usando como sistema operativo a Debian, siga las instrucciones para instalar Docker de la documentación oficial en el siguiente enlace https://docs.docker.com/engine/install/debian/#install-using-the-repository

2. Descargue los archivos de este repositorio, clonándolo.

3. Ingrese al directorio recién descargado y ejecute el siguiente comando para construir la imagen de docker:

`$ sudo docker build -t nombre_de_la_imagen:etiqueta .`

Reemplace el nombre_de_la_imagen con el nombre que desea darle a la imagen, y etiqueta con una etiqueta o versión. El punto al final del comando significa que Docker debe buscar el Dockerfile en el directorio actual. Por ejemplo:

`$ sudo docker build -t cripto:ex2 .`

4. Verifique que la imagen fue construida listando las imágenes con el siguiente comando:
`$ sudo docker image ls`

5. Una vez que la imagen se haya construido con éxito, ejecute un contenedor basado en la imagen recién creada con el siguiente comando, y que además le permitirá interactuar con una shell dentro del contenedor:
`sudo docker run --rm --interactive --tty cripto:ex2 /bin/bash`

Si está usando una imagen de Alpine para python2.7, ejecute en su lugar el siguiente comando:

`$ sudo docker run --rm --interactive --tty cripto:ex2 /bin/sh`

6. Ahora ejecute el programa de ejemplo de la expansión de llaves, el cuál se copió a la imagen a través de las instrucciones del archivo Dockerfile, por ejemplo:
`./aes_key`

7. Salga del contenedor con la combinación de teclas ctrl+d

También es posible ejecutar el programa sin ingresar al contenedor porque esas instrucciones se han configurado en el archivo Dockerfile. Entonces, en lugar de hacer los pasos 5 y 6, ejecute el siguiente comando:
`$ sudo docker run -it --rm cripto:ex2`
